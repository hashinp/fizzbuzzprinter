<?php

/**
 * FizzBuzz Printer
 * User: hash
 * Date: 28/07/2017
 * Time: 22:06
 */
class FizzBuzzPrinter
{
    private $report = [
        'lucky' => 0,
        'fizz' => 0,
        'buzz' => 0,
        'fizzbuzz' => 0,
        'integer' => 0
    ];

    public function print()
    {

        for($i = 1; $i <= 20; $i++) {
            $this->printLogic($i);
        }

        $this->printReport();
    }

    private function printLogic($i): void
    {
        if (strpos($i, '3') !== false) {
            echo 'lucky';
            $this->report['lucky']++;
        } else if ($i % 15 == 0) {
            echo 'fizzbuzz';
            $this->report['fizzbuzz']++;
        } else if ($i % 3 == 0) {
            echo 'fizz';
            $this->report['fizz']++;
        } else if ($i % 5 == 0) {
            echo 'buzz';
            $this->report['buzz']++;
        } else {
            echo $i;
            $this->report['integer']++;
        }

        if ($i < 20) {
            echo ' ';
        }
    }


    private function printReport(): void
    {
        echo "\n";
        $reportOutput = sprintf(
            'fizz: %d
buzz: %d
fizzbuzz: %d
lucky: %d
integer: %d
',
            $this->report['fizz'], $this->report['buzz'], $this->report['fizzbuzz'],
            $this->report['lucky'], $this->report['integer']
        );

        echo $reportOutput;
    }
}