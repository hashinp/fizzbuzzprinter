<?php
use PHPUnit\Framework\TestCase;

/**
 * FizzBuzzPrinter test
 * User: hash
 * Date: 28/07/2017
 * Time: 22:01
 */
class FizzBuzzPrinterTest extends TestCase
{
    private $printer;

    public function setUp()
    {
        $this->printer = new FizzBuzzPrinter();
    }

    public function testFizzBuzzPrinting()
    {
        ob_start();
        $this->printer->print();
        $outout = ob_get_clean();

        $this->assertEquals(
            '1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz
fizz: 4
buzz: 3
fizzbuzz: 1
lucky: 2
integer: 10
',
            $outout
        );
    }
}